# FisioLung2.0

* Welcome the Fisiolung repository.

This repository contain the copy of FisioLung 2.0 that published in JIS (JOURNAL ON INTERACTIVE SYSTEMS) 2023, on special issue by SBCAS 2022.

If you use FisioLung in your work, I would appreciate if you cited

@inproceedings{sbcas,

 author = {Giuliana Leon and Arthur Jardim and Érico Amaral and Julio Domingues Júnior},

 title = {Fisioterapia Respiratória: Proposta de um sistema para o monitoramento de sessões},

 booktitle = {Anais do XXII Simpósio Brasileiro de Computação Aplicada à Saúde},

 location = {Teresina},

 year = {2022},

 keywords = {},

 issn = {2763-8952},

 pages = {202--213},

 publisher = {SBC},

 address = {Porto Alegre, RS, Brasil},

 doi = {10.5753/sbcas.2022.222530},

 url = {https://sol.sbc.org.br/index.php/sbcas/article/view/21632}

}

or 

@inproceedings{JIS,

 author = {Giuliana Oliveira de Mattos Leon and William Silva Domingues and Érico Amaral and Julio Domingues Júnior},

 title = {FisioLung: A System for Training and Monitoring of Respiratory Physiotherapy Sessions},

 booktitle = {JOURNAL ON INTERACTIVE SYSTEMS 2023 - Special Issue SBCAS 2022}},

 location = {},

 year = {2023},

 keywords = {},

 issn = {2763-7719},

 pages = {},

 publisher = {},

 address = {},

 doi = {},

 url = {https://sol.sbc.org.br/journals/index.php/jis}

}


Authors:

Giuliana Oliveira de Mattos Leon [ Federal University of Pampa - UNIPAMPA | giulianaleon.aluno@unipampa.edu.br ]

William Silva Domingues [ Federal University of Pampa - UNIPAMPA | williandomingues.aluno@unipampa.edu.br ]

Érico Marcelo Hoff do Amaral [ Federal University of Pampa - UNIPAMPA | ericoamaralc@unipampa.edu.br ]

Julio Saraçol Domingues Júnior [ Federal University of Pampa - UNIPAMPA | juliodomingues@unipampa.edu.br 

Disclaimer:

This is academic software made available under the CRAPL license. For more information see the LICENSE file.

=====================================================

ory

